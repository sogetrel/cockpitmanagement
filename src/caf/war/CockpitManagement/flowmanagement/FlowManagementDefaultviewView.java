/**
 * 
 */
package caf.war.CockpitManagement.flowmanagement;

import java.util.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.component.table.html.AsyncTable;
import com.webmethods.caf.faces.component.table.html.DataTable;
import com.webmethods.caf.faces.data.object.ELPropertyContentProvider;
import com.webmethods.caf.faces.data.tree.object.ListTreeContentProvider;

import caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.Champs;
import caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.Menu;

import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */

@ManagedBean(name = "FlowManagementDefaultviewView")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "FlowManagement/default", beanType = BeanType.PAGE)
public class FlowManagementDefaultviewView  extends   com.webmethods.caf.faces.bean.BasePageBean {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 *
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/j2se/1.5.0/docs/guide/serialization/spec/version.html> 
	 * details. </a>
	 */
	
	
	
	
	
	protected ListTreeContentProvider m_menuProvider = new ListTreeContentProvider(this.getNode(), "#{row[1]}", "#{row[0]}", null) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public boolean isOpen(){
			return true;
		}
		public boolean getUseUnopenModel(){
			return true;
		}
		public Collection<?> getUnopenIds(){
			return Collections.EMPTY_LIST;
		}
	};
	
	public ListTreeContentProvider getMenuProvider(){
		return m_menuProvider;
	}
	
	private static final long serialVersionUID = 1L;
	private transient caf.war.CockpitManagement.flowmanagement.FlowManagement flowManagement = null;
	private java.lang.Boolean displayProcess;
	private java.lang.Boolean displayAsyncFlux;
	private java.lang.Boolean displayInstanceFlux;
	private java.lang.Boolean displayMessages;
	private static final String[][] ONINSTANCEFLUXSELECT_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxIn}", "#{flux.IDIn}"},
		{"#{FlowManagementDefaultviewView.lireInstancesFluxDunFlux.parameters.lireInstancesFluxDunFlux.lireInstancesFluxDunFlux.IDDemiFluxOut}", "#{flux.IDOut}"},
		{"#{FlowManagementDefaultviewView.displayInstanceFlux}", "true"},
		{"#{FlowManagementDefaultviewView.lireInstancesFluxDunFlux.refresh}", null},
		{"#{FlowManagementDefaultviewView.displayMessages}", "false"},
	};
	private java.lang.String userId;
	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	
	public String getWhoIAm() {
		return getFacesContext().getExternalContext().getRemoteUser();
	}
	
	private List<Object> getNode() {
		try{
		getLireMenus();
		this.userId =  getWhoIAm();
		this.lireMenus.getParameters().getLireMenus().getLireMenus().setPrincipal(this.userId);
		this.lireMenus.refresh();
		
		List<Object> retour = new ArrayList<Object>();
		if (this.lireMenus.getResult().getLireMenusResponse().getMenus().length != 1){
     		for(Menu m : this.lireMenus.getResult().getLireMenusResponse().getMenus()){
			 
				retour.add(new Object[]{m.getIDParent(), m.getID(), m.getTitle(), m.getEstFeuille()});
			}
		}
		else {
			retour.add(new Object[]{null, "-1", "Pas de menu, vous n'avez aucun droit, contactez le support IRIS", "0"});
		}
		return retour;
		} catch(Exception e){
			error(e);
			log(e);
		}
		return null;
	}
	
	
	
	public caf.war.CockpitManagement.flowmanagement.FlowManagement getFlowManagement()  {
		if (flowManagement == null) {
		    flowManagement = (caf.war.CockpitManagement.flowmanagement.FlowManagement)resolveExpression("#{FlowManagement}");
		}
		return flowManagement;
	}

	public String onMenuSelect() {
	    resolveDataBinding(ONMENUSELECT_PROPERTY_BINDINGS, this, "onMenuSelect.this", true, false);
		return null;
	}

	public java.lang.Boolean getDisplayProcess()  {
		
		return displayProcess;
	}

	public void setDisplayProcess(java.lang.Boolean displayProcess)  {
		this.displayProcess = displayProcess;
	}

	public java.lang.Boolean getDisplayAsyncFlux()  {
		
		return displayAsyncFlux;
	}

	public void setDisplayAsyncFlux(java.lang.Boolean displayAsyncFlux)  {
		this.displayAsyncFlux = displayAsyncFlux;
	}

	public String onProccessSelect() {
	    resolveDataBinding(ONPROCCESSSELECT_PROPERTY_BINDINGS, this, "onProccessSelect.this", true, false);
		return null;
	}

	public java.lang.Boolean getDisplayInstanceFlux()  {
		
		return displayInstanceFlux;
	}

	public void setDisplayInstanceFlux(java.lang.Boolean displayInstanceFlux)  {
		this.displayInstanceFlux = displayInstanceFlux;
	}

	public String onInstanceFluxSelect() {
	    resolveDataBinding(ONINSTANCEFLUXSELECT_PROPERTY_BINDINGS, this, "onInstanceFluxSelect.this", true, false);
		return null;
	}

	public java.lang.Boolean getDisplayMessages()  {
		
		return displayMessages;
	}

	public void setDisplayMessages(java.lang.Boolean displayMessages)  {
		this.displayMessages = displayMessages;
	}

	public String onSelectMessages() {
	    resolveDataBinding(ONSELECTMESSAGES_PROPERTY_BINDINGS, this, "onSelectMessages.this", true, false);
    	return null;
	}

	public String onSelectMessage() {
	    resolveDataBinding(ONSELECTMESSAGE_PROPERTY_BINDINGS, this, "onSelectMessage.this", true, false);
	    //onCopyClone();
		return null;
	}

	public String onResoumettre() {
	    resolveDataBinding(ONRESOUMETTRE_PROPERTY_BINDINGS, this, "onResoumettre.this", true, false);
		return null;
	}

	public java.lang.String getUserId()  {
		
		return userId;
	}

	public void setUserId(java.lang.String userId)  {
		this.userId = userId;
	}
	
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.displayProcess}", "false"},
		{"#{FlowManagementDefaultviewView.displayAsyncFlux}", "false"},
		{"#{FlowManagementDefaultviewView.displayInstanceFlux}", "false"},
		{"#{FlowManagementDefaultviewView.displayMessages}", "false"},
	};
	private java.lang.String typeFlux;
	private static final String[][] ONSELECTMESSAGES_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.displayMessages}", "true"},
		{"#{FlowManagementDefaultviewView.lireMessagesEnErreur.parameters.lireMessagesEnErreur.lireMessagesEnErreur.uuidParent}", "#{instance.UUIDMessaging}"},
		{"#{FlowManagementDefaultviewView.lireMessagesEnErreur.refresh}", null},
		{"#{FlowManagementDefaultviewView.lireEnteteMessagesEnAnomalie.parameters.lireEnteteMessagesEnAnomalie.lireEnteteMessagesEnAnomalie.IDIn}", "#{instance.IDDemiFluxIn}"},
		{"#{FlowManagementDefaultviewView.lireEnteteMessagesEnAnomalie.parameters.lireEnteteMessagesEnAnomalie.lireEnteteMessagesEnAnomalie.IDOut}", "#{instance.IDDemiFluxOut}"},
		{"#{FlowManagementDefaultviewView.lireEnteteMessagesEnAnomalie.refresh}", null},
	};
	private static final String[][] ONSELECTSYNCMESSAGES_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.displayMessages}", "true"},
		{"#{FlowManagementDefaultviewView.lireMessages.parameters.lireMessages.lireMessages.uuidParent}", "#{instance.UUIDMessaging}"},
		{"#{FlowManagementDefaultviewView.lireMessages.refresh}", null},
		{"#{FlowManagementDefaultviewView.lireEnteteMessagesEnAnomalie.parameters.lireEnteteMessagesEnAnomalie.lireEnteteMessagesEnAnomalie.IDIn}", "#{instance.IDDemiFluxIn}"},
		{"#{FlowManagementDefaultviewView.lireEnteteMessagesEnAnomalie.parameters.lireEnteteMessagesEnAnomalie.lireEnteteMessagesEnAnomalie.IDOut}", "#{instance.IDDemiFluxOut}"},
		{"#{FlowManagementDefaultviewView.lireEnteteMessagesEnAnomalie.refresh}", null},
	};
	private static final String[][] ONSELECTSYNCMESSAGE_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.lireMessages.parameters.lireMessages.lireMessages.uuidParent}", "#{messages.UUIDMessaging}"},
		{"#{FlowManagementDefaultviewView.lireMessages.refresh}", null},
	};
	private static final String[][] ONPROCCESSSELECT_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.lireFluxDunProcessus.parameters.lireFluxDunProcessus.lireFluxDunProcessus.IDProcessus}", "#{processu.id}"},
		{"#{FlowManagementDefaultviewView.displayAsyncFlux}", "#{processu.type==1 || processu.type==2}"},
		{"#{FlowManagementDefaultviewView.lireFluxDunProcessus.parameters.lireFluxDunProcessus.lireFluxDunProcessus.principal}", "#{FlowManagementDefaultviewView.userId}"},
		{"#{FlowManagementDefaultviewView.lireFluxDunProcessus.refresh}", null},
		{"#{FlowManagementDefaultviewView.displayInstanceFlux}", "false"},
		{"#{FlowManagementDefaultviewView.displayMessages}", "false"},
		{"#{FlowManagementDefaultviewView.typeFlux}", "#{processu.type}"},
	};
	private static final String[][] ONMENUSELECT_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.lireProcessus.parameters.lireProcessus.lireProcessus.domain}", "#{row[1]}"},
		{"#{FlowManagementDefaultviewView.displayProcess}", "true"},
		{"#{FlowManagementDefaultviewView.lireProcessus.refresh}", null},
		{"#{FlowManagementDefaultviewView.displayAsyncFlux}", "false"},
		{"#{FlowManagementDefaultviewView.displayInstanceFlux}", "false"},
		{"#{FlowManagementDefaultviewView.displayMessages}", "false"},
	};
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMenus lireMenus = null;
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireProcessus lireProcessus = null;
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireFluxDunProcessus lireFluxDunProcessus = null;
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux lireInstancesFluxDunFlux = null;
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireEnteteMessagesEnAnomalie lireEnteteMessagesEnAnomalie = null;
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMessagesEnErreur lireMessagesEnErreur = null;
	private static final String[][] LIREMENUS_PROPERTY_BINDINGS = new String[][] {
		{"#{lireMenus.authCredentials.authenticationMethod}", "1"},
		{"#{lireMenus.authCredentials.requiresAuth}", "true"},
		{"#{lireMenus.autoRefresh}", "false"},
		{"#{lireMenus.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireMenus\"]}"},
	};
	private static final String[][] LIREPROCESSUS_PROPERTY_BINDINGS = new String[][] {
		{"#{lireProcessus.authCredentials.authenticationMethod}", "1"},
		{"#{lireProcessus.authCredentials.requiresAuth}", "true"},
		{"#{lireProcessus.autoRefresh}", "false"},
		{"#{lireProcessus.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireProcessus\"]}"},
	};
	private static final String[][] LIREFLUXDUNPROCESSUS_PROPERTY_BINDINGS = new String[][] {
		{"#{lireFluxDunProcessus.authCredentials.authenticationMethod}", "1"},
		{"#{lireFluxDunProcessus.authCredentials.requiresAuth}", "true"},
		{"#{lireFluxDunProcessus.autoRefresh}", "false"},
		{"#{lireFluxDunProcessus.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireFlux\"]}"},
	};
	private static final String[][] LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS = new String[][] {
		{"#{lireInstancesFluxDunFlux.authCredentials.authenticationMethod}", "1"},
		{"#{lireInstancesFluxDunFlux.authCredentials.requiresAuth}", "true"},
		{"#{lireInstancesFluxDunFlux.autoRefresh}", "false"},
		{"#{lireInstancesFluxDunFlux.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireInstance\"]}"},
	};
	private static final String[][] LIREENTETEMESSAGESENANOMALIE_PROPERTY_BINDINGS = new String[][] {
		{"#{lireEnteteMessagesEnAnomalie.authCredentials.authenticationMethod}", "1"},
		{"#{lireEnteteMessagesEnAnomalie.authCredentials.requiresAuth}", "true"},
		{"#{lireEnteteMessagesEnAnomalie.autoRefresh}", "false"},
		{"#{lireEnteteMessagesEnAnomalie.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireEnteteMessage\"]}"},
	};
	private static final String[][] LIREMESSAGESENERREUR_PROPERTY_BINDINGS = new String[][] {
		{"#{lireMessagesEnErreur.authCredentials.authenticationMethod}", "1"},
		{"#{lireMessagesEnErreur.authCredentials.requiresAuth}", "true"},
		{"#{lireMessagesEnErreur.autoRefresh}", "false"},
		{"#{lireMessagesEnErreur.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireMessagesAvecErreur\"]}"},
	};
	private transient caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMessageAvecPivot lireMessageAvecPivot = null;
	private static final String[][] LIREMESSAGEAVECPIVOT_PROPERTY_BINDINGS = new String[][] {
		{"#{lireMessageAvecPivot.authCredentials.authenticationMethod}", "1"},
		{"#{lireMessageAvecPivot.authCredentials.requiresAuth}", "true"},
		{"#{lireMessageAvecPivot.autoRefresh}", "false"},
		{"#{lireMessageAvecPivot.endpointAddress}", "#{environment[\"wsclient-endpointAddress-lireMessage\"]}"},
	};
	private transient com.webmethods.caf.faces.data.object.ListTableContentProvider messageProvider = null;
	private static final String[][] MESSAGEPROVIDER_PROPERTY_BINDINGS = new String[][] {
		{"#{MessageProvider.rowType}", "caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub$Champs"},
		{"#{MessageProvider.rowVariable}", "champ"},
	};
	private static final String[][] ONSELECTMESSAGE_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.lireMessageAvecPivot.parameters.lireMessageAvecPivot.lireMessageAvecPivot.UUID}", "#{messagesEnAnomalie.UUIDMessaging}"},
		{"#{FlowManagementDefaultviewView.lireMessageAvecPivot.refresh}", null},
		{"#{FlowManagementDefaultviewView.messageProvider.array}", "#{FlowManagementDefaultviewView.lireMessageAvecPivot.result.lireMessageAvecPivotResponse.message.champs}"},
	};
	private static final String[][] ONRESOUMETTRE_PROPERTY_BINDINGS = new String[][] {
		{"#{FlowManagementDefaultviewView.resoumettreMessageavecCorrection.parameters.resoumettreMessageavecCorrection.resoumettreMessageavecCorrection.message.UUIDMessaging}", "#{FlowManagementDefaultviewView.lireMessageAvecPivot.result.lireMessageAvecPivotResponse.message.UUIDMessaging}"},
		{"#{FlowManagementDefaultviewView.resoumettreMessageavecCorrection.parameters.resoumettreMessageavecCorrection.resoumettreMessageavecCorrection.message.IDDemiFluxIn}", "#{FlowManagementDefaultviewView.lireMessageAvecPivot.result.lireMessageAvecPivotResponse.message.IDDemiFluxIn}"},
		{"#{FlowManagementDefaultviewView.resoumettreMessageavecCorrection.parameters.resoumettreMessageavecCorrection.resoumettreMessageavecCorrection.message.IDDemiFluxOut}", "#{FlowManagementDefaultviewView.lireMessageAvecPivot.result.lireMessageAvecPivotResponse.message.IDDemiFluxOut}"},
		{"#{FlowManagementDefaultviewView.resoumettreMessageavecCorrection.parameters.resoumettreMessageavecCorrection.resoumettreMessageavecCorrection.message.champs}", "#{FlowManagementDefaultviewView.messageProvider.array}"},
		{"#{FlowManagementDefaultviewView.resoumettreMessageavecCorrection.refresh}", null},
		{"#{FlowManagementDefaultviewView.lireInstancesFluxDunFlux.refresh}", null},
		{"#{FlowManagementDefaultviewView.displayMessages}", "false"},
	};
	private transient caf.war.CockpitManagement.wsclient.wscockpit.LireMessages lireMessages = null;
	private static final String[][] LIREMESSAGES_PROPERTY_BINDINGS = new String[][] {
		{"#{LireMessages.authCredentials.authenticationMethod}", "1"},
		{"#{LireMessages.authCredentials.requiresAuth}", "true"},
	};
	private transient caf.war.CockpitManagement.wsclient.wscockpit.ResoumettreMessageavecCorrection resoumettreMessageavecCorrection = null;
	private static final String[][] RESOUMETTREMESSAGEAVECCORRECTION_PROPERTY_BINDINGS = new String[][] {
		{"#{ResoumettreMessageavecCorrection.authCredentials.authenticationMethod}", "1"},
		{"#{ResoumettreMessageavecCorrection.authCredentials.requiresAuth}", "true"},
	};
	public java.lang.String getTypeFlux()  {
		
		return typeFlux;
	}


	public void setTypeFlux(java.lang.String typeFlux)  {
		this.typeFlux = typeFlux;
	}


	public String onSelectSyncMessages() {
	    resolveDataBinding(ONSELECTSYNCMESSAGES_PROPERTY_BINDINGS, this, "onSelectSyncMessages.this", true, false);
		return null;
	}


	public String onSelectSyncMessage() {
	    resolveDataBinding(ONSELECTSYNCMESSAGE_PROPERTY_BINDINGS, this, "onSelectSyncMessage.this", true, false);
		return null;
	}


	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMenus getLireMenus()  {
		if (lireMenus == null) {
		    lireMenus = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMenus)resolveExpression("#{LireMenus}");
		}
	
	    resolveDataBinding(LIREMENUS_PROPERTY_BINDINGS, lireMenus, "lireMenus", false, false);
		return lireMenus;
	}


	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireProcessus getLireProcessus()  {
		if (lireProcessus == null) {
		    lireProcessus = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireProcessus)resolveExpression("#{LireProcessus}");
		}
	
	    resolveDataBinding(LIREPROCESSUS_PROPERTY_BINDINGS, lireProcessus, "lireProcessus", false, false);
		return lireProcessus;
	}


	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireFluxDunProcessus getLireFluxDunProcessus()  {
		if (lireFluxDunProcessus == null) {
		    lireFluxDunProcessus = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireFluxDunProcessus)resolveExpression("#{LireFluxDunProcessus}");
		}
	
	    resolveDataBinding(LIREFLUXDUNPROCESSUS_PROPERTY_BINDINGS, lireFluxDunProcessus, "lireFluxDunProcessus", false, false);
		return lireFluxDunProcessus;
	}


	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux getLireInstancesFluxDunFlux()  {
		if (lireInstancesFluxDunFlux == null) {
		    lireInstancesFluxDunFlux = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireInstancesFluxDunFlux)resolveExpression("#{LireInstancesFluxDunFlux}");
		}
	
	    resolveDataBinding(LIREINSTANCESFLUXDUNFLUX_PROPERTY_BINDINGS, lireInstancesFluxDunFlux, "lireInstancesFluxDunFlux", false, false);
		return lireInstancesFluxDunFlux;
	}


	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireEnteteMessagesEnAnomalie getLireEnteteMessagesEnAnomalie()  {
		if (lireEnteteMessagesEnAnomalie == null) {
		    lireEnteteMessagesEnAnomalie = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireEnteteMessagesEnAnomalie)resolveExpression("#{LireEnteteMessagesEnAnomalie}");
		}
	
	    resolveDataBinding(LIREENTETEMESSAGESENANOMALIE_PROPERTY_BINDINGS, lireEnteteMessagesEnAnomalie, "lireEnteteMessagesEnAnomalie", false, false);
		return lireEnteteMessagesEnAnomalie;
	}


	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMessagesEnErreur getLireMessagesEnErreur()  {
		if (lireMessagesEnErreur == null) {
		    lireMessagesEnErreur = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMessagesEnErreur)resolveExpression("#{LireMessagesEnErreur}");
		}
	
	    resolveDataBinding(LIREMESSAGESENERREUR_PROPERTY_BINDINGS, lireMessagesEnErreur, "lireMessagesEnErreur", false, false);
		return lireMessagesEnErreur;
	}


	private String onCopyClone()
	{
		
		try{
			
		
			if (this.getLireMessageAvecPivot().getResult().getLireMessageAvecPivotResponse().getMessage().getChamps().length != 1){
				for ( Champs chps : this.getLireMessageAvecPivot().getResult().getLireMessageAvecPivotResponse().getMessage().getChamps()){
					Champs cop = new Champs();
					cop.setNom(chps.getNom());
					cop.setValeur(chps.getValeur());
					this.getMessageProvider().getList().add(cop);
				}
			}

		} catch(Exception e){
		error(e);
		log(e);
		}
		return null;
	}
	
	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMessageAvecPivot getLireMessageAvecPivot()  {
		if (lireMessageAvecPivot == null) {
		    lireMessageAvecPivot = (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.LireMessageAvecPivot)resolveExpression("#{LireMessageAvecPivot}");
		}
	
	    resolveDataBinding(LIREMESSAGEAVECPIVOT_PROPERTY_BINDINGS, lireMessageAvecPivot, "lireMessageAvecPivot", false, false);
		return lireMessageAvecPivot;
	}


	public com.webmethods.caf.faces.data.object.ListTableContentProvider getMessageProvider()  {
		if (messageProvider == null) {
		    messageProvider = (com.webmethods.caf.faces.data.object.ListTableContentProvider)resolveExpression("#{MessageProvider}");
		}
	
	    resolveDataBinding(MESSAGEPROVIDER_PROPERTY_BINDINGS, messageProvider, "messageProvider", false, false);
		return messageProvider;
	}


	public void setMessageProvider(com.webmethods.caf.faces.data.object.ListTableContentProvider messageProvider)  {
		this.messageProvider = messageProvider;
	}


	public caf.war.CockpitManagement.wsclient.wscockpit.LireMessages getLireMessages()  {
		if (lireMessages == null) {
		    lireMessages = (caf.war.CockpitManagement.wsclient.wscockpit.LireMessages)resolveExpression("#{LireMessages}");
		}
	
	    resolveDataBinding(LIREMESSAGES_PROPERTY_BINDINGS, lireMessages, "lireMessages", false, false);
		return lireMessages;
	}


	public caf.war.CockpitManagement.wsclient.wscockpit.ResoumettreMessageavecCorrection getResoumettreMessageavecCorrection()  {
		if (resoumettreMessageavecCorrection == null) {
		    resoumettreMessageavecCorrection = (caf.war.CockpitManagement.wsclient.wscockpit.ResoumettreMessageavecCorrection)resolveExpression("#{ResoumettreMessageavecCorrection}");
		}
	
	    resolveDataBinding(RESOUMETTREMESSAGEAVECCORRECTION_PROPERTY_BINDINGS, resoumettreMessageavecCorrection, "resoumettreMessageavecCorrection", false, false);
		return resoumettreMessageavecCorrection;
	}
	
}