/**
 * 
 */
package caf.war.CockpitManagement.flowmanagement;

/**
 * @author vital.thyot
 *
 */

import javax.portlet.PortletPreferences;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

@ManagedBean(name = "FlowManagement")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(displayName = "FlowManagement", beanType = BeanType.PORTLET)
public class FlowManagement  extends   com.webmethods.caf.faces.bean.BaseFacesPreferencesBean {

	public static final String[] PREFERENCES_NAMES = new String[] {};
	private transient caf.war.CockpitManagement.CockpitManagement cockpitManagement = null;
	
	/**
	 * Create new preferences bean with list of preference names
	 */
	public FlowManagement() {
		super(PREFERENCES_NAMES);
	}
	
	/**
	 * Call this method in order to persist
	 * Portlet preferences
	 */
	public void storePreferences() throws Exception {
		updatePreferences();
		PortletPreferences preferences = getPreferences();
		preferences.store();
	}

	public caf.war.CockpitManagement.CockpitManagement getCockpitManagement()  {
		if (cockpitManagement == null) {
		    cockpitManagement = (caf.war.CockpitManagement.CockpitManagement)resolveExpression("#{CockpitManagement}");
		}
		return cockpitManagement;
	}
}