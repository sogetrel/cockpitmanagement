/**
 * 
 */
package caf.war.CockpitManagement;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * @author vital.thyot
 *
 */
@ManagedBean(name = "CockpitManagement")
@ApplicationScoped
@DTManagedBean(displayName = "CockpitManagement", beanType = BeanType.APPLICATION)
public class CockpitManagement extends com.webmethods.caf.faces.bean.BaseApplicationBean 
{
	public CockpitManagement()
	{
		super();
		setCategoryName( "CafApplication" );
		setSubCategoryName( "CockpitManagement" );
	}
}