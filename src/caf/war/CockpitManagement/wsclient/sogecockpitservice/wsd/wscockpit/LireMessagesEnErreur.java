package caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.lireMessagesEnErreur.
 */
@ManagedBean(name = "LireMessagesEnErreur")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class LireMessagesEnErreur extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 1238965987310744576L;
	
	/**
	 * Constructor
	 */
	public LireMessagesEnErreur() {
		super(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"lireMessagesEnErreur", // method to invoke
			new String[] { "lireMessagesEnErreur", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 188114784192220160L;
		
		public Parameters() {
		}
	
	  
		private caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesEnErreurE lireMessagesEnErreur  = new  caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesEnErreurE() ;

		public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesEnErreurE getLireMessagesEnErreur() {
			return lireMessagesEnErreur;
		}

		public void setLireMessagesEnErreur(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesEnErreurE lireMessagesEnErreur) {
			this.lireMessagesEnErreur = lireMessagesEnErreur;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesEnErreurResponseE getResult() {
		return (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesEnErreurResponseE)result;
	}
	
	
}