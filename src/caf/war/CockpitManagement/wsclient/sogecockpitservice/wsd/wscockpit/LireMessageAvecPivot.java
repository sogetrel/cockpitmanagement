package caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.lireMessageAvecPivot.
 */
@ManagedBean(name = "LireMessageAvecPivot")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class LireMessageAvecPivot extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 737636728414485504L;
	
	/**
	 * Constructor
	 */
	public LireMessageAvecPivot() {
		super(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"lireMessageAvecPivot", // method to invoke
			new String[] { "lireMessageAvecPivot", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 1458613092101202944L;
		
		public Parameters() {
		}
	
	  
		private caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessageAvecPivotE lireMessageAvecPivot  = new  caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessageAvecPivotE() ;

		public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessageAvecPivotE getLireMessageAvecPivot() {
			return lireMessageAvecPivot;
		}

		public void setLireMessageAvecPivot(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessageAvecPivotE lireMessageAvecPivot) {
			this.lireMessageAvecPivot = lireMessageAvecPivot;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessageAvecPivotResponseE getResult() {
		return (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessageAvecPivotResponseE)result;
	}
	
	
}