package caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.lireEnteteMessagesEnAnomalie.
 */
@ManagedBean(name = "LireEnteteMessagesEnAnomalie")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class LireEnteteMessagesEnAnomalie extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 1276385941828665344L;
	
	/**
	 * Constructor
	 */
	public LireEnteteMessagesEnAnomalie() {
		super(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"lireEnteteMessagesEnAnomalie", // method to invoke
			new String[] { "lireEnteteMessagesEnAnomalie", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 1220024936516665344L;
		
		public Parameters() {
		}
	
	  
		private caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireEnteteMessagesEnAnomalieE lireEnteteMessagesEnAnomalie  = new  caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireEnteteMessagesEnAnomalieE() ;

		public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireEnteteMessagesEnAnomalieE getLireEnteteMessagesEnAnomalie() {
			return lireEnteteMessagesEnAnomalie;
		}

		public void setLireEnteteMessagesEnAnomalie(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireEnteteMessagesEnAnomalieE lireEnteteMessagesEnAnomalie) {
			this.lireEnteteMessagesEnAnomalie = lireEnteteMessagesEnAnomalie;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireEnteteMessagesEnAnomalieResponseE getResult() {
		return (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireEnteteMessagesEnAnomalieResponseE)result;
	}
	
	
}