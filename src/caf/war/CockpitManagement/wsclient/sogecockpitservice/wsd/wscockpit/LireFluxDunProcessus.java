package caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit;


import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.webmethods.caf.faces.annotations.ExpireWithPageFlow;
import com.webmethods.caf.faces.annotations.DTManagedBean;
import com.webmethods.caf.faces.annotations.BeanType;

/**
 * Web Service Client bean generated for 
 * caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.lireFluxDunProcessus.
 */
@ManagedBean(name = "LireFluxDunProcessus")
@SessionScoped
@ExpireWithPageFlow
@DTManagedBean(beanType = BeanType.DEFAULT)
public class LireFluxDunProcessus extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 381885091919647744L;
	
	/**
	 * Constructor
	 */
	public LireFluxDunProcessus() {
		super(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"lireFluxDunProcessus", // method to invoke
			new String[] { "lireFluxDunProcessus", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 5859746129570647040L;
		
		public Parameters() {
		}
	
	  
		private caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireFluxDunProcessusE lireFluxDunProcessus  = new  caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireFluxDunProcessusE() ;

		public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireFluxDunProcessusE getLireFluxDunProcessus() {
			return lireFluxDunProcessus;
		}

		public void setLireFluxDunProcessus(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireFluxDunProcessusE lireFluxDunProcessus) {
			this.lireFluxDunProcessus = lireFluxDunProcessus;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireFluxDunProcessusResponseE getResult() {
		return (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireFluxDunProcessusResponseE)result;
	}
	
	
}