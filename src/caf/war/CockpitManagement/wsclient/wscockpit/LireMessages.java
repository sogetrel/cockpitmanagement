package caf.war.CockpitManagement.wsclient.wscockpit;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.lireMessages.
 */
public class LireMessages extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 62677269592790016L;
	
	/**
	 * Constructor
	 */
	public LireMessages() {
		super(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"lireMessages", // method to invoke
			new String[] { "lireMessages", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 5736724173045559296L;
		
		public Parameters() {
		}
	
	  
		private caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesE lireMessages  = new  caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesE() ;

		public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesE getLireMessages() {
			return lireMessages;
		}

		public void setLireMessages(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesE lireMessages) {
			this.lireMessages = lireMessages;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	/**
	 * Return method invocation result bean
	 */
	public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesResponseE getResult() {
		return (caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.LireMessagesResponseE)result;
	}
	
	
}