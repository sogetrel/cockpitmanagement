package caf.war.CockpitManagement.wsclient.wscockpit;


import java.io.Serializable;

/**
 * Web Service Client bean generated for 
 * caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.resoumettreMessageavecCorrection.
 */
public class ResoumettreMessageavecCorrection extends com.webmethods.caf.faces.data.ws.wss.WSSContentProvider {

	private static final long serialVersionUID = 657162766381375488L;
	
	/**
	 * Constructor
	 */
	public ResoumettreMessageavecCorrection() {
		super(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.class,  // port type proxy class
			"resoumettreMessageavecCorrection", // method to invoke
			new String[] { "resoumettreMessageavecCorrection", } // method parameter names
		);
		
		// init wsclient
		initParams();
		
		
		// parameters bean
		parameters = new Parameters();
			
		// initial result
		result = null;
	}
	
	
	/**
	 * Method parameters bean
	 */
	public class Parameters implements Serializable {

		private static final long serialVersionUID = 2808207014344751104L;
		
		public Parameters() {
		}
	
	  
		private caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.ResoumettreMessageavecCorrectionE resoumettreMessageavecCorrection  = new  caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.ResoumettreMessageavecCorrectionE() ;

		public caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.ResoumettreMessageavecCorrectionE getResoumettreMessageavecCorrection() {
			return resoumettreMessageavecCorrection;
		}

		public void setResoumettreMessageavecCorrection(caf.war.CockpitManagement.wsclient.sogecockpitservice.wsd.wscockpit.SOGECockpitServiceWsdWsCockpitStub.ResoumettreMessageavecCorrectionE resoumettreMessageavecCorrection) {
			this.resoumettreMessageavecCorrection = resoumettreMessageavecCorrection;
		}
		
	}
	
	/**
	 * Return method invocation parameters bean
	 */
	public Parameters getParameters() {
		return (Parameters)parameters;
	}	
	


	
	
}